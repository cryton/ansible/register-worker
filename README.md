# Ansible role - Register Worker
This role registers Cryton Worker in Cryton Core using Cryton CLI.

## Supported platforms

| Platform | version   |
|----------|-----------|
| Debian   | \>=11     |
| Kali     | \>=2022.1 |

## Requirements
Cryton Core and Cryton CLI must be installed.

## Parameters
The parameters and their defaults can be found in the `defaults/main.yml` file.

It is recommended to update the `cryton_cli_environment` variable.

For example:
```yaml
      cryton_cli_environment:
        MY_VARIABLE: my_value
```

| variable                  | description                             |
|---------------------------|-----------------------------------------|
| cryton_cli_executable     | Path of the Cryton CLI executable.      |
| run_as_user               | Which user to use.                      |
| cryton_cli_environment    | Temporary override Cryton CLI settings. |
| cryton_worker_name        | Worker's name.                          |
| cryton_worker_description | Description of the Worker.              |

## Examples

### Usage
**CLI is installed using pip:**
```yaml
  roles:
    - role: register-worker
      cryton_worker_name: Worker
      cryton_worker_description: base description

```

**CLI is installed using Docker:**
```yaml
  roles:
    - role: register-worker
      become: yes
      run_as_user: root
      cryton_cli_executable: docker exec cryton-cli cryton-cli
      cryton_worker_name: Worker
      cryton_worker_description: base description

```

### Inclusion

[https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies](https://docs.ansible.com/ansible/latest/galaxy/user_guide.html#dependencies)
```yaml
- name: register-worker
  src: https://gitlab.ics.muni.cz/cryton/ansible/register-worker.git
  version: "master"
  scm: git
```
